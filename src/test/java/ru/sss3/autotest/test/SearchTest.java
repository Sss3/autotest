package ru.sss3.autotest.test;

import org.testng.annotations.Test;
import ru.sss3.autotest.framework.AbstractTest;
import ru.sss3.autotest.framework.base.Content;

/**
 * @author sss3
 */
@Test
public class SearchTest extends AbstractTest {

    private final static String SINGLE_FILM_NAME = "Тесты для настоящих мужчин";

    public void testSingleResultSearch() {
        Content content = baseHelper.searchFilms(SINGLE_FILM_NAME);
        assert content.isFilmPage() : "Открылась не страница фильма";
        assert content.getFilmPage().getName().equals(SINGLE_FILM_NAME);
    }

    public void testManyResultSearch() {
        assert baseHelper.searchFilms("Тест").getFilms().size() > 1 : "Открылась не страница результатов";
    }

}
