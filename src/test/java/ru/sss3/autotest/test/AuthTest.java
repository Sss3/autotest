package ru.sss3.autotest.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import ru.sss3.autotest.framework.AbstractTest;
import ru.sss3.autotest.framework.RangomGenerator;
import ru.sss3.autotest.framework.base.LogPage;
import ru.sss3.autotest.framework.base.RegPage;

import static ru.sss3.autotest.framework.RangomGenerator.getRandomString;

/**
 * @author sss3
 */
@Test
public class AuthTest extends AbstractTest {

    public void testIncorrectMailRegistration() {
        String randomStringWithoutAtSign = getRandomString(5);
        RegPage regPage = navigator.regPage();
        regPage.insertEmail(randomStringWithoutAtSign);
        regPage.registration();
        assert regPage.getErrorString().contains("Неверный e-mail!") : "Не сработала валидация email";
    }

    @Test(alwaysRun = true, dependsOnMethods = "testIncorrectMailRegistration")
    public void testIncorrectPasswordOnLogin() {
        navigator.rootPage();
        LogPage logPage = navigator.logPage();
        logPage.insertLogin(getRandomString(5));
        logPage.insertPassword(getRandomString(5));
        logPage.login();
        assert logPage.hasErrorString() : "Нет сообщения ошибки";
    }

    @Test(alwaysRun = true, dependsOnMethods = "testIncorrectPasswordOnLogin")
    public void correctLogin() {
        navigator.rootPage();
        LogPage logPage = navigator.logPage();
        logPage.insertLogin(props.getProperty("login"));
        logPage.insertPassword(props.getProperty("password"));
        logPage.login();

        WebElement logout = webDriver.findElement(By.cssSelector("#b_logout"));
        assert logout != null && logout.isDisplayed() : "Авторизация не прошла успешно";
    }
}
