package ru.sss3.autotest.test;

import org.testng.annotations.Test;
import ru.sss3.autotest.framework.AbstractTest;
import ru.sss3.autotest.framework.base.FilmPage;
import ru.sss3.autotest.framework.base.FilmPreview;

import java.util.List;

/**
 * @author sss3
 */
@Test
public class LastPremierFilmsTest extends AbstractTest {

    private final static int PREMIER_BLOCK_SIZE = 7;

    public void testSize() {
        assert baseHelper.getLastPremierBlock().size() == PREMIER_BLOCK_SIZE : "Неверное количество фильмов в блоке премьер";
    }

    public void testCorrectWork() {
        List<FilmPreview> lastPremierBlock = baseHelper.getLastPremierBlock();
        FilmPreview randomFilm = lastPremierBlock.get((int) (Math.random() * lastPremierBlock.size()));
        String name = randomFilm.getName();
        FilmPage filmPage = randomFilm.openAndGetFilmPage();
        assert name.equals(filmPage.getName()) : "Открылся не тот фильм";
    }

}
