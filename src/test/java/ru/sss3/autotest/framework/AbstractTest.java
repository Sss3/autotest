package ru.sss3.autotest.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author sss3
 */
public abstract class AbstractTest {

    private final static Object lock = new Object();

    protected WebDriver webDriver;
    public static Properties props;

    protected BaseHelper baseHelper;
    protected NavigatorHelper navigator;

    @BeforeSuite
    public void initProps() {
        if (props == null) {
            synchronized (lock) {
                if (props == null) {
                    props = new Properties();
                    try(InputStream in = AbstractTest.class.getClassLoader().getResourceAsStream("autotest.properties")) {
                        props.load(in);
                    } catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                }
            }
        }
    }

    @BeforeClass
    public void before() {
        webDriver = WebDriverPool.getInstance().get();
        baseHelper = new BaseHelper(webDriver);
        navigator = new NavigatorHelper(webDriver);
    }

    @AfterClass
    public void after() {
        WebDriverPool.getInstance().release(webDriver);
    }

    @AfterSuite
    public void afterSuite() {
        WebDriverPool.getInstance().finish();
    }

    private final static class WebDriverPool {
        private int size = Integer.valueOf(props.getProperty("maxDriver"));
        private int currentSize = 0;
        private final static Object lock = new Object();
        private final LinkedBlockingQueue<WebDriver> drivers = new LinkedBlockingQueue<>(size);

        static {
            System.setProperty("webdriver.chrome.driver", "C:/study/chromedriver.exe");
        }

        private static WebDriverPool INSTANCE;

        private WebDriverPool() {

        }

        static WebDriverPool getInstance() {
            if (INSTANCE == null) {
                synchronized (lock) {
                    if (INSTANCE == null) {
                        INSTANCE = new WebDriverPool();
                    }
                }
            }
            return INSTANCE;
        }

        WebDriver get() {
            if (drivers.isEmpty() && currentSize + 1 <= size) {
                currentSize++;
                try {
                    drivers.put(init());
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
            return drivers.poll();
        }

        void release(WebDriver driver) {
            try {
                driver.get(props.getProperty("baseUrl"));
                drivers.put(driver);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }

        void finish() {
            drivers.forEach(WebDriver::close);
        }

        private WebDriver init() {
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(props.getProperty("baseUrl"));
            return driver;
        }
    }

}
