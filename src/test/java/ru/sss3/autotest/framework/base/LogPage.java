package ru.sss3.autotest.framework.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author sss3
 */
public class LogPage {

    private final WebDriver driver;

    public LogPage(WebDriver driver) {
        this.driver = driver;
        driver.switchTo().frame(driver.findElement(By.className("iframp")));
    }

    public void insertLogin(String login) {
        driver.findElement(By.cssSelector("#user_login")).sendKeys(login);
    }

    public void insertPassword(String password) {
        driver.findElement(By.cssSelector("#user_pass")).sendKeys(password);
    }

    public void login() {
        driver.findElement(By.cssSelector("#wp-submit")).click();
    }

    public boolean hasErrorString() {
        return driver.findElement(By.id("login_error")).isDisplayed();
    }
}
