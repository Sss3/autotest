package ru.sss3.autotest.framework.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author sss3
 */
public class FilmPreview {

    private boolean isTopFilms;
    private final WebElement element;
    private final WebDriver driver;

    public FilmPreview(boolean isTopFilms, WebElement element, WebDriver driver) {
        this.isTopFilms = isTopFilms;
        this.element = element;
        this.driver = driver;
    }

    public FilmPage openAndGetFilmPage() {
        element.click();
        return new FilmPage(driver);
    }

    public String getName() {
        return isTopFilms ? element.getText().trim() : element.findElement(By.cssSelector("span")).getText().trim();
    }
}
