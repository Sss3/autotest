package ru.sss3.autotest.framework.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author sss3
 */
public class FilmPage {

    private final WebDriver driver;

    public FilmPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getName() {
        return driver.findElement(By.cssSelector("#single > div.t-row > div:nth-child(1) > div.rl-2 > h1")).getText().trim();
    }
}
