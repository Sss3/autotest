package ru.sss3.autotest.framework.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.sss3.autotest.framework.BaseHelper;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author sss3
 */
public class Content {
    private final WebDriver driver;

    public Content(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isFilmPage() {
        return BaseHelper.elementPresent(() -> driver.findElement(By.id("face")));
    }

    public List<FilmPreview> getFilms() {
        assert !isFilmPage();
        return driver.findElements(By.className("mainlink")).stream()
                .map(el -> new FilmPreview(false, el, driver))
                .collect(toList());
    }

    public FilmPage getFilmPage() {
        assert isFilmPage();
        return new FilmPage(driver);
    }
}
