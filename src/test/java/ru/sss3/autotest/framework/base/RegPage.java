package ru.sss3.autotest.framework.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author sss3
 */
public class RegPage {

    private final WebDriver driver;

    public RegPage(WebDriver driver) {
        this.driver = driver;
        driver.switchTo().frame(driver.findElement(By.className("iframp")));
    }

    public void insertLogin(String login) {
        throw new IllegalStateException();
    }

    public void insertEmail(String email) {
        driver.findElement(By.id("user_email")).sendKeys(email);
    }

    public void registration() {
        driver.findElement(By.cssSelector("#wp-submit")).click();
//        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#wp-submit")));
    }

    public String getErrorString() {
        return driver.findElement(By.id("login_error")).getText().trim();
    }

}
