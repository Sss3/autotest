package ru.sss3.autotest.framework;

/**
 * @author sss3
 */
public class RangomGenerator {

    private final static String CHARS = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";

    public static String getRandomString(int charsCount) {
        StringBuilder sb = new StringBuilder(charsCount);
        for (int i = 0; i < charsCount; i++) {
            sb.append(CHARS.charAt((int) (Math.random() * CHARS.length())));
        }
        return sb.toString();
    }

}
