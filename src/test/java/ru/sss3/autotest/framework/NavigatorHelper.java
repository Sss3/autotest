package ru.sss3.autotest.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.sss3.autotest.framework.base.LogPage;
import ru.sss3.autotest.framework.base.RegPage;

/**
 * @author sss3
 */
public class NavigatorHelper {

    private final WebDriver driver;

    public NavigatorHelper(WebDriver driver) {
        this.driver = driver;
    }

    public RegPage regPage() {
        driver.findElement(By.cssSelector("#reg_button > span")).click();
        return new RegPage(driver);
    }

    public LogPage logPage() {
        driver.findElement(By.cssSelector("#in_button > span")).click();
        return new LogPage(driver);
    }

    public void rootPage() {
        driver.get(AbstractTest.props.getProperty("baseUrl"));
    }
}
