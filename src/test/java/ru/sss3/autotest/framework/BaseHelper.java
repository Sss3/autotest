package ru.sss3.autotest.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.sss3.autotest.framework.base.Content;
import ru.sss3.autotest.framework.base.FilmPreview;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * @author sss3
 */
public class BaseHelper {

    private final WebDriver driver;

    public BaseHelper(WebDriver driver) {
        this.driver = driver;
    }

    public List<FilmPreview> getLastPremierBlock() {
        return driver.findElements(By.className("toplink")).stream()
                .map(element -> new FilmPreview(true, element, driver)).collect(toList());
    }

    public Content searchFilms(String name) {
        driver.findElement(By.cssSelector("#s")).sendKeys(name);
        driver.findElement(By.cssSelector("#btnSearch")).click();
        return new Content(driver);
    }

    public static boolean elementPresent(Supplier<WebElement> elementSupplier) {
        try {
            return elementSupplier.get() != null;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}
